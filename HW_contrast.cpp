// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_contrast:
//
// Apply contrast enhancement to I1. Output is in I2.
// Stretch intensity difference from reference value (128) by multiplying
// difference by "contrast" and adding it back to 128. Shift result by
// adding "brightness" value.
//
// Written by: Ian S. McBride, Enan Rahman, Fioger Shahollari, 2016
//
void
HW_contrast(ImagePtr I1, double brightness, double contrast, ImagePtr I2)
{
  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();
  int total = w * h;

  // init lookup table
  double tmp_double;
  int i, tmp_int, lut[MXGRAY];
  int ref_value = MXGRAY/2;
  for(i=0; i<MXGRAY; ++i) {
    // evaluate contrast/brightness forumla for current intensity
    tmp_double = ((i - ref_value) * contrast) + ref_value + brightness;
    // round, then clip, then store result
    tmp_int = ROUND(tmp_double);
    lut[i] = CLIP(tmp_int, 0, MaxGray);
  }

  int type;
  ChannelPtr<uchar> p1, p2, endd;

  // for each channel
  for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
    IP_getChannel(I2, ch, p2, type);
    // evaluate output: each input pixel indexes into lut[] to eval output
    for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
  }
}
