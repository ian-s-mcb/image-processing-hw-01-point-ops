// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_clip:
//
// Clip intensities of image I1 to [t1,t2] range. Output is in I2.
// If    input<t1: output = t1;
// If t1<input<t2: output = input;
// If      val>t2: output = t2;
//
// Written by: Ian S. McBride, Enan Rahman, Fioger Shahollari, 2016
//
void
HW_clip(ImagePtr I1, int t1, int t2, ImagePtr I2)
{
    IP_copyImageHeader(I1, I2);
    int w = I1->width ();
    int h = I1->height();
    int total = w * h;

    int type;
    ChannelPtr<uchar> p1, p2, endd;

    // for each channel
    for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
      IP_getChannel(I2, ch, p2, type);

      // evaluate output: use the provided macro for clipping
      for(endd = p1 + total; p1<endd; p1++, p2++) {
        *p2 = CLIP(*p1, t1, t2);
      }
    }
}
