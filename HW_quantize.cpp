// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_quantize:
//
// Quantize I1 to specified number of levels. Apply dither if flag is set.
// Output is in I2.
//
// Written by: Ian S. McBride, Enan Rahman, Fioger Shahollari, 2016
//
void
HW_quantize(ImagePtr I1, int levels, bool dither, ImagePtr I2)
{
  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();
  int total = w * h;

  /* init lookup table;
  Scale specifies the difference in intensity between each quantization
  level. Bias is half of scale, and it is used to shift the LUT value
  from the bottom of each quantization level to the midpoint. */
  int i, lut[MXGRAY];
  int scale = MXGRAY / levels;
  int bias = scale / 2;
  for(i=0; i<MXGRAY; i++)
    lut[i] = scale * (i/scale) + bias;

  int type;
  ChannelPtr<uchar> p1, p2, endd;

  if(dither) {
    int i, j, noise, noiseSign, tmp;

    // for each channel
    for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
      IP_getChannel(I2, ch, p2, type);
      /* evaluate output;
      Each input pixel indexes into lut[] to eval output.

      Noise is added to each input intensity value before it is passed
      into lut[]. The noise is random, but limited in amplitude to bias.
      The sign of noise alternates from positive to negative after each
      iteration of the two for loops. The alternative sign of the noise
      gives a checkerboard arrangement.*/
      for(i = 0, noiseSign = 1; i < h; i++, noiseSign *= -1) {
        for(j = 0; j < w; j++, noiseSign *= -1) {
          noise = noiseSign * bias * ((double)rand() / RAND_MAX);
          tmp = *p1++ + noise;
          tmp = CLIP(tmp, 0, MaxGray);
          *p2++ = lut[tmp];
        }
      }
    }
  }
  else {
    // for each channel
    for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
      IP_getChannel(I2, ch, p2, type);
      // evaluate output: each input pixel indexes into lut[] to eval output
      for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
    }
  }
}
