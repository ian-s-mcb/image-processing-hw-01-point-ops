// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_gammaCorrect:
//
// Gamma correct image I1. Output is in I2.
//
// Written by: Ian S. McBride, Enan Rahman, Fioger Shahollari, 2016
//
void
HW_gammaCorrect(ImagePtr I1, double gamma, ImagePtr I2)
{
  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();
  int total = w * h;

  // init lookup table
  int i, lut[MXGRAY];
  double exponent = 1 / gamma;
  double tmp;
  for(i=0; i<MXGRAY; ++i) {
    /* Put current intensity value in range [0, 1], then raise value to
    the exponent, then scale the value to [0, 255]. */
    tmp = pow((double)i / MaxGray, exponent) * MaxGray;
    lut[i] = tmp;
  }

  int type;
  ChannelPtr<uchar> p1, p2, endd;

  // for each channel
  for(int ch = 0; IP_getChannel(I1, ch, p1, type); ch++) {
    IP_getChannel(I2, ch, p2, type);

    // evaluate output: each input pixel indexes into lut[] to eval output
    for(endd = p1 + total; p1<endd;) *p2++ = lut[*p1++];
  }
}
